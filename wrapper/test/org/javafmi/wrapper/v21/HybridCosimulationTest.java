/*
 *  Copyright 2013-2016 SIANI - ULPGC
 *
 *  This File is part of JavaFMI Project
 *
 *  JavaFMI Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  JavaFMI Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with JavaFMI. If not, see <http://www.gnu.org/licenses/>.
 */

package org.javafmi.wrapper.v21;

import org.javafmi.proxy.FmiProxyV21;
import org.javafmi.proxy.FmiProxyV21.HybridDoStepStatus;
import org.javafmi.proxy.Status;
import org.javafmi.wrapper.Simulation;
import org.junit.Test;

import static org.javafmi.kernel.OS.isWin64;
import static org.junit.Assume.assumeTrue;

public class HybridCosimulationTest {

	@Test
	public void hybrid_test_using_c1() throws Exception {
		assumeTrue(isWin64());
		Simulation simulation = new Simulation(HybridCosimulationTest.class.getResource("/fmu/cosimulation/v21/C1.fmu").getFile());
		Access access = new Access(simulation);
		simulation.init(0);

		double currentTime = 0;
		for (int step = 0; step < 10; step++) {
			System.out.println("Next event will be at: " + access.getNextEventTime());
			HybridDoStepStatus hybridDoStepStatus = access.hybridDoStep(currentTime, 1);
			currentTime = hybridDoStepStatus.getEndTime();
			System.out.print("HybridDoStep status --> " + hybridDoStepStatus.getStatus());
			System.out.print("\toccurredEvent --> " + hybridDoStepStatus.isOccurredEvent());
			System.out.print("\tendTime --> " + hybridDoStepStatus.getEndTime());
			if (hybridDoStepStatus.isOccurredEvent()) {
				FmiProxyV21.RealsAtEvent valve = access.getRealAtEvent(currentTime, simulation.getModelDescription().getValueReference("Valve"));
				System.out.print("\tValve before event --> " + valve.getBeforeEvent()[0]);
				System.out.println("\tValve after event --> " + valve.getAfterEvent()[0]);
			} else
				System.out.println();
		}

		simulation.terminate();
	}

	@Test
	public void hybrid_test_using_c1_bis() throws Exception {
		assumeTrue(isWin64());
		Simulation simulation = new Simulation(HybridCosimulationTest.class.getResource("/fmu/cosimulation/v21/C1bis.fmu").getFile());
		Access access = new Access(simulation);
		simulation.init(0);

		double currentTime = 0;
		for (int step = 0; step < 10; step++) {
			System.out.println("Next event will be at: " + access.getNextEventTime());
			HybridDoStepStatus hybridDoStepStatus = access.hybridDoStep(currentTime, 1);
			currentTime = hybridDoStepStatus.getEndTime();
			System.out.print("HybridDoStep status --> " + hybridDoStepStatus.getStatus());
			System.out.print("\toccurredEvent --> " + hybridDoStepStatus.isOccurredEvent());
			System.out.print("\tendTime --> " + hybridDoStepStatus.getEndTime());
			if (hybridDoStepStatus.isOccurredEvent()) {
				System.out.print("\tbefore values(valve=" + simulation.read("Valve").asBoolean() + ", size=" +
						simulation.read("Size").asInteger() + ", level=" + simulation.read("Level").asDouble() + ")");
				access.hybridDoStep(currentTime, 0);
				System.out.println("\tafter values(valve=" + simulation.read("Valve").asBoolean() + ", size=" +
						simulation.read("Size").asInteger() + ", level=" + simulation.read("Level").asDouble() + ")");
			} else
				System.out.println();
		}
		simulation.terminate();
	}

	@Test
	public void newDiscreteStates_using_c1Bis2() throws Exception {
		assumeTrue(isWin64());
		Simulation simulation = new Simulation(HybridCosimulationTest.class.getResource("/fmu/cosimulation/v21/C1-bis2.fmu").getFile());
		Access access = new Access(simulation);
		simulation.init(0);

		double currentTime = 0;
		for (int step = 0; step < 10; step++) {
			FmiProxyV21.EventInfo info = new FmiProxyV21.EventInfo(true, false, false, false, false, 0.0);
			System.out.println("Next event will be at: " + access.getNextEventTime());
			HybridDoStepStatus hybridDoStepStatus = access.hybridDoStep(currentTime, 1);
			access.newDiscreteStates(info);
			currentTime = hybridDoStepStatus.getEndTime();
			System.out.print("HybridDoStep status --> " + hybridDoStepStatus.getStatus());
			System.out.print("\toccurredEvent --> " + hybridDoStepStatus.isOccurredEvent());
			System.out.print("\tendTime --> " + hybridDoStepStatus.getEndTime());

			if (hybridDoStepStatus.isOccurredEvent()) {
				System.out.print("\tValve before event --> " + simulation.read("Valve").asDouble());
				info = new FmiProxyV21.EventInfo(true, false, false, false, false, 0.0);
				while (info.newDiscreteStatesNeeded)
					access.newDiscreteStates(info);
				System.out.print("\tValve after event --> " + simulation.read("Valve").asDouble());
				System.out.println("\tNext event time--> " + info.nextEventTime());
			} else
				System.out.println();
		}

		simulation.terminate();
	}


}