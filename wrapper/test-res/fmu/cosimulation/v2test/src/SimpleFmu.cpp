#include "../include/fmi2Functions.h"
#include <stdio.h>
#include <stdlib.h>

#define MAX_REAL_VARIABLES 5
#define MAX_INTEGER_VARIABLES 5
#define MAX_BOOLEAN_VARIABLES 5
#define MAX_STRING_VARIABLES 5
#define REALS_OFFSET 10
#define INTEGERS_OFFSET 20
#define BOOLEANS_OFFSET 30
#define STRING_OFFSET 40
#define DEFAULT_FMU_SIZE 128

int component;
int state;
double reals[MAX_REAL_VARIABLES];
int integers[MAX_INTEGER_VARIABLES];
int booleans[MAX_BOOLEAN_VARIABLES];
const char* strings[MAX_STRING_VARIABLES];

const char* fmi2GetTypesPlatform(){
	return "default";
}

fmi2Component fmi2Instantiate(fmi2String instanceName, fmi2Type fmuType, fmi2String fmuGUID, fmi2String fmuResourceLocation, const fmi2CallbackFunctions* functions, fmi2Boolean visible, fmi2Boolean loggingOn){
	printf("call to fmi2Instantiate %s, %s, %s \n", instanceName, fmuGUID, fmuResourceLocation);
	printf("\tTesting callbacks\n");
	printf("\t\tTesting allocate\n");
	char* nameForMessage = (char*)functions->allocateMemory(128, sizeof(char));
	char* category = (char*)functions->allocateMemory(128, sizeof(char));
	char* message = (char*)functions->allocateMemory(128, sizeof(char));
	sprintf(nameForMessage, "nameForMessage");
	sprintf(category, "category");
	sprintf(message, "message");
	printf("\t\tTesting logger\n");
	functions->logger(0, nameForMessage, fmi2OK, category, "Calling log with: 0, 1, 2, 4", 0, 1, 2, 4);
	functions->logger(0, nameForMessage, fmi2OK, category, "Calling log with no args");
	functions->logger(0, nameForMessage, fmi2OK, category, "Calling log with: 11", 11);
	functions->logger(0, nameForMessage, fmi2OK, category, "Calling log with: 0, 1", 0, 1);
	functions->logger(0, nameForMessage, fmi2OK, category, "Calling log with: 0, 1, 2", 0, 1, 2);
	functions->logger(0, nameForMessage, fmi2OK, category, "Calling log with 0, 1, 2, 4, string", 0, 1, 2, 4, "string");
	printf("\t\tTesing freeMemory\n");
	functions->freeMemory(nameForMessage);
	functions->freeMemory(category);
	functions->freeMemory(message);
	return &component;
}

void fmi2FreeInstance(fmi2Component component){
	printf("fmi2FreeInstance\n");
}

fmi2Status fmi2SetDebugLogging(fmi2Component component, fmi2Boolean loggingOn, size_t nCategories, const fmi2String categories[]){
	printf("fmi2SetDebugLogging\n");
	return fmi2OK;
}

fmi2Status fmi2SetupExperiment(fmi2Component component, fmi2Boolean toleranceDefined, fmi2Real tolerance, fmi2Real startTime, fmi2Boolean stopTimeDefined, fmi2Real stopTime){
	printf("fmi2SetupExperiment tolerance: %f, start time: %f, stop time: %f\n", tolerance, startTime, stopTime);
	return fmi2OK;
}

fmi2Status fmi2EnterInitializationMode(fmi2Component component){
	printf("fmi2EnterInitializationMode\n");
	return fmi2OK;
}

fmi2Status fmi2ExitInitializationMode(fmi2Component component){
	printf("fmi2ExitInitializationMode\n");
	return fmi2OK;
}

fmi2Status fmi2Terminate(fmi2Component component){
	printf("fmi2Terminate\n");
	return fmi2OK;
}

fmi2Status fmi2Reset(fmi2Component component){
	printf("fmi2Reset\n");
	return fmi2OK;
}

fmi2Status fmi2GetReal(fmi2Component component, const fmi2ValueReference valueReferences[], size_t numberOfValueReferences, fmi2Real values[]){
	printf("fmi2GetReal: {");
	if (numberOfValueReferences > MAX_REAL_VARIABLES) return fmi2Error;
	for (int i=0; i<numberOfValueReferences; i++){
		values[i] = reals[valueReferences[i] - REALS_OFFSET];
		printf("%d: %f, ", valueReferences[i], values[i]);
	}
	printf("}\n");
	return fmi2OK;
}

fmi2Status fmi2GetInteger(fmi2Component component, const fmi2ValueReference valueReferences[], size_t numberOfValueReferences, fmi2Integer values[]){
	printf("fmi2GetInteger: {");
	if (numberOfValueReferences > MAX_INTEGER_VARIABLES) return fmi2Error;
	for (int i=0; i<numberOfValueReferences; i++){
		values[i] = integers[valueReferences[i] - INTEGERS_OFFSET];
		printf("%d: %d, ", valueReferences[i], values[i]);
	}
	printf("}\n");
	return fmi2OK;
}

fmi2Status fmi2GetBoolean(fmi2Component component, const fmi2ValueReference valueReferences[], size_t numberOfValueReferences, fmi2Boolean values[]){
	printf("fmi2GetBoolean: {");
	if (numberOfValueReferences > MAX_BOOLEAN_VARIABLES) return fmi2Error;
	for (int i=0; i<numberOfValueReferences; i++){
		values[i] = booleans[valueReferences[i] - BOOLEANS_OFFSET];
		printf("%d: %d, ", valueReferences[i], values[i]);
	}
	printf("}\n");
	return fmi2OK;
}

fmi2Status fmi2GetString(fmi2Component component, const fmi2ValueReference valueReferences[], size_t numberOfValueReferences, fmi2String values[]){
	printf("fmi2GetString: {");
	if (numberOfValueReferences > MAX_STRING_VARIABLES) return fmi2Error;
	for (int i=0; i<numberOfValueReferences; i++){
		values[i] = strings[valueReferences[i] - STRING_OFFSET];
		printf("%d: \"%s\", ", valueReferences[i], values[i]);
	}
	printf("}\n");
	return fmi2OK;
}

fmi2Status fmi2SetReal(fmi2Component component, const fmi2ValueReference valueReferences[], size_t numberOfValueReferences, const fmi2Real values[]){
	printf("fmi2SetReal: {");
	if (numberOfValueReferences > MAX_REAL_VARIABLES) return fmi2Error;
	for (int i=0; i<numberOfValueReferences; i++){
		reals[valueReferences[i] - REALS_OFFSET] = values[i];
		printf("%d: %f, ", valueReferences[i], values[i]);
	}
	printf("}\n");
	return fmi2OK;
}

fmi2Status fmi2SetInteger(fmi2Component component, const fmi2ValueReference valueReferences[], size_t numberOfValueReferences, const fmi2Integer values[]){
	printf("fmi2SetInteger: {");
	if (numberOfValueReferences > MAX_INTEGER_VARIABLES) return fmi2Error;
	for (int i=0; i<numberOfValueReferences; i++){
		integers[valueReferences[i] - INTEGERS_OFFSET] = values[i];
		printf("%d: %d, ", valueReferences[i], values[i]);
	}
	printf("}\n");
	return fmi2OK;
}

fmi2Status fmi2SetBoolean(fmi2Component component, const fmi2ValueReference valueReferences[], size_t numberOfValueReferences, const fmi2Boolean values[]){
	printf("fmi2SetBoolean: {");
	if (numberOfValueReferences > MAX_BOOLEAN_VARIABLES) return fmi2Error;
	for (int i=0; i<numberOfValueReferences; i++){
		booleans[valueReferences[i] - BOOLEANS_OFFSET] = values[i];
		printf("%d: %d, ", valueReferences[i], values[i]);
	}
	printf("}\n");
	return fmi2OK;
}

fmi2Status fmi2SetString(fmi2Component component, const fmi2ValueReference valueReferences[], size_t numberOfValueReferences, const fmi2String values[]){
	printf("fmi2SetString: ");
	if (numberOfValueReferences > MAX_STRING_VARIABLES) return fmi2Error;
	for (int i=0; i<numberOfValueReferences; i++){
		strings[valueReferences[i] - STRING_OFFSET] = values[i];
		printf(" {%d: \"%s\"} ", valueReferences[i], values[i]);
	}
	printf("\n");
	return fmi2OK;
}

fmi2Status fmi2GetFMUstate(fmi2Component component, fmi2FMUstate* state){
	printf("fmi2GetFMUstate\n");
	return fmi2OK;
}

fmi2Status fmi2SetFMUstate(fmi2Component component, fmi2FMUstate  state){
	printf("fmi2SetFMUstate\n");
	return fmi2OK;
}

fmi2Status fmi2FreeFMUstate(fmi2Component component, fmi2FMUstate* state){
	printf("fmi2FreeFMUstate\n");
	return fmi2OK;
}

fmi2Status fmi2SerializedFMUstateSize(fmi2Component component, fmi2FMUstate state, size_t* size){
	printf("fmi2SerializedFMUstateSize\n");
	*size = DEFAULT_FMU_SIZE;
	return fmi2OK;
}

fmi2Status fmi2SerializeFMUstate(fmi2Component component, fmi2FMUstate state, fmi2Byte serializedState[], size_t size){
	printf("fmi2SerializeFMUState with size %lu\n", size);
	for (int i=0; i<size; i++)
		serializedState[i] = i;
	return fmi2OK;
}

fmi2Status fmi2DeSerializeFMUstate(fmi2Component component, const fmi2Byte serializedState, size_t size, fmi2FMUstate* state){
	printf("fmi2DeSerializeFMUState\n");
	return fmi2OK;
}

fmi2Status fmi2GetDirectionalDerivative(fmi2Component component, const fmi2ValueReference unknownValueReferences[], size_t numberOfUnknowns, const fmi2ValueReference knownValueReferences[], int numberOfKnowns, double knownDifferential[], double unknownDifferential[]){
	printf("fmi2GetDirectionalDerivative ");
	for (int i = 0; i < numberOfKnowns; i++){
		printf("(unknown:%d, known:%d , knownDv:%f) ", unknownValueReferences[i], knownValueReferences[i], knownDifferential[i]);
	}
	printf("\n");
	return fmi2OK;
}

fmi2Status fmi2SetRealInputDerivatives(fmi2Component component, const fmi2ValueReference valueReferences[], size_t numberOfValueReferences, fmi2Integer orders[], fmi2Real values[]){
	printf("fmi2SetRealInputDerivatives ");
	for (int i = 0; i < numberOfValueReferences; i++){
		printf("(value reference:%d, order:%d, value:%f) ", valueReferences[i], orders[i], values[i]);
	}
	printf("\n");
	return fmi2OK;
}

fmi2Status fmi2GetRealOutputDerivatives(fmi2Component component, const fmi2ValueReference valueReferences[], fmi2Integer numberOfValueReferences, const fmi2Integer orders[], fmi2Real values[]){
	printf("fmi2GetRealOutputDerivatives ");
	for (int i = 0; i < numberOfValueReferences; i++){
		printf("(value reference:%d, order:%d) ", valueReferences[i], orders[i]);
	}
	printf("\n");
	return fmi2OK;	
}

fmi2Status fmi2DoStep(fmi2Component component, fmi2Real currentCommunicationPoint, fmi2Real communicationStepSize, fmi2Boolean noSetFMUStatePriorToCurrentPoint){
	printf("fmi2DoStep currentCommunicationPoint:%f, communicationStepSize:%f, noSetFMUStatePriorToCurrentPoint:%d\n", currentCommunicationPoint, communicationStepSize, noSetFMUStatePriorToCurrentPoint);
	return fmi2OK;
}

fmi2Status fmi2CancelStep(fmi2Component component){
	printf("fmi2CancelStep\n");
	return fmi2OK;
}

fmi2Status fmi2GetStatus(fmi2Component component, const fmi2StatusKind statusKind, fmi2Status* status){
	printf("fmi2GetStatus\n");
	*status = fmi2Pending;
	return fmi2OK;
}

fmi2Status fmi2GetRealStatus(fmi2Component component, const fmi2StatusKind statusKind, fmi2Real* status){
	printf("fmi2GetRealStatus\n");
	*status = 0.;
	return fmi2OK;
}

fmi2Status fmi2GetIntegerStatus(fmi2Component component, const fmi2StatusKind statusKind, fmi2Integer* status){
	printf("fmi2GetIntegerStatus\n");
	*status = 1;
	return fmi2OK;
}

fmi2Status fmi2GetBooleanStatus(fmi2Component component, const fmi2StatusKind statusKind, fmi2Boolean* status){
	printf("fmi2GetBooleanStatus\n");
	*status = fmi2False;
	return fmi2OK;
}

fmi2Status fmi2GetStringStatus(fmi2Component component, const fmi2StatusKind statusKind, fmi2String* status){
	printf("fmi2GetStringStatus\n");
	*status = "fake status";
	return fmi2OK;
}
