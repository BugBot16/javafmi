/*

 Copyright 2013, 2014 SIANI - ULPGC
 Jose Juan Hernandez Cabrera
 Jose Evora Gomez
 Johan Sebastian Cortes Montenegro
 Maria del Carmen Sánchez Medrano

 This File is Part of JavaFMI Project

 JavaFMI Project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 JavaFMI Project is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with JavaFMI Library.  If not, see <http://www.gnu.org/licenses/>.

 */

import org.javafmi.Car;
import org.javafmi.exporter.framework.FmiAccess;
import org.javafmi.exporter.framework.modeldescription.ModelVariables;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.javafmi.exporter.framework.FmiAccess.Status;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class CarTest {

	@Test
	public void createsTheTripSimulation() {
		Car car = new Car();
		car.init();
		car.setSpeed(40);
		car.doStep(1800);
		Double distanceAfterHalfHour = car.getDistance();
		car.doStep(1800);
		Double distanceAfterAnHour = car.getDistance();
		Status terminateStatus = car.terminate();

		assertEquals(Status.OK, terminateStatus);
		assertEquals(20000, distanceAfterHalfHour, 0.1);
		assertEquals(40000, distanceAfterAnHour, 0.1);
	}

	@Test
	public void variablesCanBeSetBeforeInit() {
		FmiAccess car = new Car();
		ModelVariables modelVariables = car.getModelDescription().getModelVariables();
		int carSpeed = modelVariables.valueReference("car.speed");
		int carDistance = modelVariables.valueReference("car.distance");

		car.setReal(new int[]{carSpeed}, new double[]{40.});
		car.setReal(new int[]{carDistance}, new double[]{10.});
		car.fmiEnterInitializationMode();
		car.fmiExitInitializationMode();

		assertThat(car.getReal(carDistance)[0], is(10.));
		assertThat(car.getReal(carSpeed)[0], is(40.));
		car.terminate();
	}

	@Test
	public void getStateAndSetStateShouldWork() {
		Car car = new Car();
		car.init();
		assertThat(car.getDistance(), is(0.0));
		car.getState("state0");
		car.setSpeed(40);
		car.doStep(1);
		assertEquals(car.getDistance(), 11.1, 0.1);
		car.setState("state0");
		assertThat(car.getDistance(), is(0.0));
	}

}
