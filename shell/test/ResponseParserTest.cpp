﻿#include "catch.hpp"
#include "fakeit.hpp"
#include "../src/shelllibrary/ResponseParser.h"
#include "../src/shelllibrary/ErrorFileHandler.h"
#include <string>

using std::string;
using namespace fakeit;

TEST_CASE("ResponseParserTest") {
	Mock<ErrorFileHandler> error_handler;
	When(Method(error_handler, copyErrorFileTo)).AlwaysReturn();
	When(Dtor(error_handler)).AlwaysReturn();
	ResponseParser parser{shared_ptr<ErrorFileHandler>(&error_handler.get())};

	SECTION("stracting status") {
		REQUIRE(parser.extractStatus("ok") == fmi2OK) ;
		REQUIRE(parser.extractStatus("warning") == fmi2Warning) ;
		REQUIRE(parser.extractStatus("discard") == fmi2Discard) ;
		REQUIRE(parser.extractStatus("error") == fmi2Error) ;
		REQUIRE(parser.extractStatus("pending") == fmi2Pending) ;
		REQUIRE(parser.extractStatus("fatal") == fmi2Fatal) ;
	}

	SECTION("stracting real") {
		double value;
		parser.extractReal("ok 3.141592", &value);
		REQUIRE(value == 3.141592) ;

		parser.extractReal("ok 4e-3", &value);
		REQUIRE(value == 0.004) ;
	}

	SECTION("stracting integer") {
		int value;
		parser.extractInteger("ok 123456", &value);
		REQUIRE(value == 123456) ;
	}

	SECTION("stracting boolean") {
		int value;
		parser.extractBoolean("ok false", &value);
		REQUIRE(value == 0) ;
		parser.extractBoolean("ok true", &value);
		REQUIRE(value == 1) ;
	}

	SECTION("stracting string") {
		char value[5];
		parser.extractString("ok foo", value);
		REQUIRE(value[0] == 'f') ;
		REQUIRE(value[1] == 'o') ;
		REQUIRE(value[2] == 'o') ;
		REQUIRE(value[3] == 0) ;
	}

	SECTION("copy error file when return code is error, warning or fatal") {
		parser.extractStatus("ok any other values");
		parser.extractStatus("warning any other values");
		parser.extractStatus("discard any other values");
		parser.extractStatus("error any other values");
		parser.extractStatus("pending any other values");
		parser.extractStatus("fatal any other values");
		Verify(Method(error_handler, copyErrorFileTo)).Exactly(3);
	}
}
