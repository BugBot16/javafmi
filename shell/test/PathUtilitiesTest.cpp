#include "catch.hpp"
#include "fakeit.hpp"
#include "../src/shelllibrary/PathUtilities.h"

TEST_CASE("PathUtilitiesTest") {

	SECTION("removes protocol when url is valid") {
		string valid_url{ "file:///valid/url/resources/" };
		auto resources_path = extractResourcesPath(valid_url, [](string& any) {return true; });
#if defined(_WIN64)
		REQUIRE(resources_path == "valid/url/resources");
#else
		REQUIRE(resources_path == "/valid/url/resources");
#endif

	}

	SECTION("throws invalid exception when invalid resources directory") {
		string invalid_url{ "file:///any/url/" };
		auto stub_checker = [](string& resources_path) { return false; };
		REQUIRE_THROWS_AS(extractResourcesPath(invalid_url, stub_checker), InvalidResourcesDirectory);
	}

	SECTION("ataches resources string to path when initial url is not valid") {
		string resources_is_inside_directory{ "file:///resources/is/inside" };
		auto stub_checker = [](string& resources_path) {
			string resources{ "resources" };
			return  (resources_path.substr(resources_path.length() - resources.length(), resources.length()) == resources);
		};
		auto resources_path = extractResourcesPath(resources_is_inside_directory, stub_checker);
#if defined(_WIN64)
		REQUIRE(resources_path == "resources/is/inside/resources");
#else
		REQUIRE(resources_path == "/resources/is/inside/resources");
#endif
	}
}