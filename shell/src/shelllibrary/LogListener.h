#pragma once

#include "Poco/Thread.h"
#include <memory>
#include "LogSource.h"
#include "Logger.h"

using std::shared_ptr;

using namespace Poco;

class LogListener: Runnable {
	shared_ptr<LogSource> log_source;
	shared_ptr<Logger> logger;
	bool continue_listening = true;
	Thread listen_thread;

public:
	LogListener(shared_ptr<LogSource> log_source, shared_ptr<Logger> logger);
	void listen();
	void stop();
	void keepListening(bool value);
private:
	void run() override;
};
