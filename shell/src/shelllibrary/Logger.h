#pragma once

#include <string>
#include <fmi/fmi2Functions.h>

using std::string;

class Logger {
	fmi2CallbackLogger output;
public:
	explicit Logger(fmi2CallbackLogger output);
	virtual ~Logger();
	virtual void log(string message);
};