#pragma once
#include <fmi/fmi2FunctionTypes.h>
#include <string>
#include "ErrorFileHandler.h"
#include <map>
#include <memory>

using std::string;
using std::shared_ptr;
using std::map;

class ResponseParser {
	shared_ptr<ErrorFileHandler> error_file_handler;
	map<string, fmi2Status> codes;
public:
	const static string OK_CODE;
	const static string WARNING_CODE;
	const static string DISCARD_CODE;
	const static string ERROR_CODE;
	const static string PENDING_CODE;
	const static string FATAL_CODE;

	ResponseParser(shared_ptr<ErrorFileHandler> error_file_handler);

	fmi2Status extractStatus(const string& string);
	fmi2Status extractReal(const string& response, fmi2Real doubleValue[]);
	fmi2Status extractInteger(const string& response, fmi2Integer intValue[]);
	fmi2Status extractBoolean(const string& response, fmi2Boolean booleanValue[]);
	fmi2Status extractString(const string& response, char* stringValue);
};
