﻿#include <string.h>
#include "LogSource.h"

LogSource::LogSource(shared_ptr<istream> input_stream) : input_stream{input_stream} { }

LogSource::~LogSource() { }

string LogSource::getLine() {
    memset(buffer, 0, MAX_BUFFER_SIZE);
    input_stream->getline(buffer, MAX_BUFFER_SIZE);
    return string{buffer};
}