#pragma once

#include <string>
#include <memory>
#include <Poco/PipeStream.h>
#include <Poco/Process.h>

using namespace Poco;
using std::shared_ptr;
using std::string;

#define BUFFER_SIZE 1024
class CommunicationHandle {
	shared_ptr<ProcessHandle> process;
	shared_ptr<PipeOutputStream> process_in;
	shared_ptr<PipeInputStream> process_out;
	shared_ptr<PipeInputStream> process_error;
	char buffer[BUFFER_SIZE];
public:
	CommunicationHandle(shared_ptr<ProcessHandle> process, shared_ptr<PipeOutputStream> process_in, shared_ptr<PipeInputStream> process_out, shared_ptr<PipeInputStream> process_error);

	void sendMessage(const string& message);

	string receiveMessage();

	shared_ptr<std::istream> outChannel();
};

