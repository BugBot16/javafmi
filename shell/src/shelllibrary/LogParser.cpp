#include <vector>
#include "LogParser.h"
#include <sstream>

using std::vector;
using std::stringstream;

LogParser::LogParser(string input) :input(input) {
	stringstream input_as_stream(input);
	string item;
	vector<string> tokens;
	while (getline(input_as_stream, item, ':')) {
		tokens.push_back(item);
	}
	if (tokens.size() == 3) {
		instance_name = tokens[0];
		_category = tokens[1];
		_text = tokens[2];
	} else
		_text = input;

}

string LogParser::instanceName() {
	return instance_name;
}

string LogParser::category() {
	return _category;
}

string LogParser::text() {
	return _text;
}